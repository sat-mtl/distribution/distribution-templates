<!-- markdownlint-disable MD041 -->
## Summary

Should be closed when all these tasks are resolved:

| Issue  | Merge request | Name         | Status           |
|--------|---------------|--------------|------------------|
| #issue | !request(s)   | Resolve this | ~"Ready to test" |
|        |               |              |                  |

## Checklists

All tasks are documented in the [release handbook](https://sat-mtl.gitlab.io/valorisation/internal-docs/guides/release/).

### Requirements

- [ ] Create a milestone `<project> <version>`
- [ ] Tag all issues panned for this release
- [ ] Create an issue with a release overview

### Starting

- [ ] Create the branch `candidate/x.x.x`
- [ ] Create a merge request from `candidate/x.x.x` to `master`

### Updating

- [ ] Rebase candidate branch onto `develop`
- [ ] Update all files:
  - [ ] CHANGELOG.md
  - [ ] AUTHORS.md
  - [ ] README.md
- [ ] Update the software version in
  - [ ] package.json
  - [ ] README.md
- [ ] Make announcement in the QA channel with the information:
  - [ ] All added fixes and features
  - [ ] A link to this issue with an updated overview

### Ending

- [ ] Update all files:
  - [ ] CHANGELOG.md
  - [ ] AUTHORS.md
  - [ ] README.md
- [ ] Update the software version in
  - [ ] package.json
  - [ ] README.md
- [ ] Merge the MR from the candidate branch to `master` or `main`
- [ ] Tag the production branch with
  - [ ] the current version
  - [ ] the last CHANGELOG
- [ ]  Create a merge request from `master` to `develop`
  - [ ] Merge back every change
- [ ] Close the milestone and this issue

/milestone %"<milestone>"

/label ~"in progress"
/label ~"type::release candidate"
/weight 2

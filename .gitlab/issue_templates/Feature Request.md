<!-- markdownlint-disable MD041 -->
## Summary

(Summarize your feature request concisely.)

## How would you like the feature to work ?

(If you have a proposition about how the feature should be offered, or how it can be implemented, it can be described here.)

## Why is this feature important to you ?

(Describe here the positive impact for you if this feature was made available.)

## Impact of non-completion

(Describe here issues related to the non completion of the requested feature.)

## Useful resources

(Add links to examples, guides, tools, specifications or documents related to your request.)

/label ~"on hold"
/label ~"type::feature"
